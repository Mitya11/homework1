using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float WalkSpeed = 4f;
    public float CrouchWalkSpeed = 2f;
    public float MidairSteerAccel = 1f;
    public float JumpForce = 100f;

    public float Health = 100f;

    interface IMoveMode
    {
        void OnJump();
        void OnCrouch();
        float GetSpeed(float direction);
    }

    IMoveMode currentMoveMode;

    IMoveMode moveWalk;
    IMoveMode moveCrouch;
    IMoveMode moveMidair;
    IMoveMode moveHook;

    float distToGround;
    float distToWall;
    Vector3 halfLenght;
    bool wasInMidair;
    bool wasInHook;
    Rigidbody rigidBody;
    float lastVelocityX;
    float lastVelocityY;
    float jumpVelocityX;
    float hookcooldown;
    void Awake()
    {
        moveWalk = new MoveModeWalk { owner = this };
        moveCrouch = new MoveModeCrouch { owner = this };
        moveMidair = new MoveModeMidair { owner = this };
        moveHook = new MoveModeHooked { owner = this };
        currentMoveMode = moveWalk;
        rigidBody = GetComponent<Rigidbody>();

        var bounds = GetComponent<BoxCollider>().bounds;
        distToGround = bounds.extents.y + 0.05f;
        distToWall = bounds.extents.x + 0.05f;
        halfLenght = new Vector3(bounds.extents.x, 0, 0);
        hookcooldown = 0;
    }

    //Should be called in Update() to properly process inputs and calculate expected velocity
    public void Move(float direction, bool jumpRequested, bool crouchRequested)
    {
        if(jumpRequested)
            currentMoveMode.OnJump();
        else if(crouchRequested)
            currentMoveMode.OnCrouch();

        JumpUpdate();
        
        Vector3 delta = new Vector3();
        float velocityX = currentMoveMode.GetSpeed(direction);
        delta.x += Time.fixedDeltaTime * velocityX;

        lastVelocityX = velocityX;
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        var lava = collisionInfo.gameObject.GetComponent<Lava>();
        if(lava == null)
            return;
        //OnCollisionStay called during physics update, using fixedDeltaTime
        Health -= lava.DamagePerSecond * Time.fixedDeltaTime; 
    }

    //Applying calculated velocity in FixedUpdate so it's in sync with the rest of physics engine
    void FixedUpdate()
    {
        hookcooldown -= Time.fixedDeltaTime;
        var velocity = rigidBody.velocity;
        velocity.x = lastVelocityX;
        rigidBody.velocity = velocity;
        Debug.Log(lastVelocityX);
    }

    void JumpStart(float force,float direction = 0)
    {
        HookStop();
        Vector3 Vforce = new Vector3(0, force, 0);
        lastVelocityX = direction * 0.03f ;
        rigidBody.AddForce(Vforce);
        jumpVelocityX = lastVelocityX;
        currentMoveMode = moveMidair;
        wasInMidair = true;
    }

    void JumpLand()
    {
        currentMoveMode = moveWalk;
        wasInMidair = false;
    }
    void HookStart() {
        wasInMidair = false;
        currentMoveMode = moveHook;
        var velocity = rigidBody.velocity;
        velocity.y = 0;
        rigidBody.velocity = velocity;
        rigidBody.useGravity = false;
        Debug.Log(currentMoveMode);
        wasInHook = true;

    }
    void HookStop()
    {
        hookcooldown = 0.3f;
        rigidBody.useGravity = true;
        currentMoveMode = moveMidair;
        wasInHook = false;
        wasInMidair = true;
    }
    void JumpUpdate()
    {
        if (wasInMidair && IsInWall() && !wasInHook)
        {
            HookStart();
        }
        else if (!wasInMidair && IsInMidair()&& !IsInWall())
        {
            if (currentMoveMode == moveCrouch) { moveCrouch.OnJump(); }
            JumpStart(force: 0f); //start a free fall if we reach a cliff
        }
        else if (wasInMidair && !IsInMidair())
            JumpLand(); //land once we reach the ground
        
    }

    void CrouchingStart()
    {
        transform.localScale = Vector3.one;
        transform.localPosition += 0.5f*Vector3.down;
        currentMoveMode = moveCrouch;
    }

    void CrouchingStop()
    {
        transform.localScale = new Vector3(1.0f, 2.0f, 1.0f);
        transform.localPosition += 0.5f*Vector3.up;
        currentMoveMode = moveWalk;
    }
    
    bool IsInMidair()
    {
        var pos = transform.position;
        //We are in midair if neither our front nor our back is on the ground
        return
            !Physics.Raycast(pos + halfLenght, Vector3.down, out _, distToGround) &&
            !Physics.Raycast(pos - halfLenght, Vector3.down, out _, distToGround);
    }
    bool IsInWall()
    {
        if (hookcooldown > 0) { return false; }
        var pos = transform.position;
        //We are in midair if neither our front nor our back is on the ground
        return
            Physics.Raycast(pos, Vector3.left, out _, distToWall) ||
            Physics.Raycast(pos, Vector3.right, out _, distToWall);
    }

    abstract class MoveModeBase : IMoveMode
    {
        public PlayerController owner;
        
        public abstract void OnJump();
        public abstract void OnCrouch();
        public abstract float GetSpeed(float direction);
    }

    class MoveModeWalk : MoveModeBase
    {
        public override void OnJump()
        {
            owner.JumpStart(owner.JumpForce);
        }

        public override void OnCrouch()
        {
            owner.CrouchingStart();
        }

        public override float GetSpeed(float direction)
        {
            return direction*owner.WalkSpeed;
        }
    }

    class MoveModeCrouch : MoveModeBase
    {
        //Both jump and crouch buttons make you stop crouching 
        public override void OnJump()
        {
            owner.CrouchingStop();
        }

        public override void OnCrouch()
        {
            owner.CrouchingStop();
        }

        public override float GetSpeed(float direction)
        {
            return direction*owner.CrouchWalkSpeed;
        }
    }
    class MoveModeHooked : MoveModeBase
    {
        //Both jump and crouch buttons make you stop crouching 
        public override void OnJump()
        {
            var pos = owner.transform.position;
            owner.HookStop();
            if (Physics.Raycast(pos, Vector3.left, out _, owner.distToWall)){
                owner.JumpStart(owner.JumpForce,owner.JumpForce);
            }
            else {
                owner.JumpStart(owner.JumpForce, -1*owner.JumpForce);
            }
            
        }

        public override void OnCrouch()
        {
            owner.HookStop();
        }

        public override float GetSpeed(float direction)
        {

            return direction * owner.CrouchWalkSpeed;
        }
    }
    class MoveModeMidair : MoveModeBase
    {
        //Not allowing neither double jumps nor crouching in midair for now, doing nothing here
        public override void OnJump() { }
        public override void OnCrouch() { }

        public override float GetSpeed(float direction)
        {
            
            owner.jumpVelocityX += direction * owner.MidairSteerAccel * Time.fixedDeltaTime;
            return owner.jumpVelocityX;
        }
    }
}
